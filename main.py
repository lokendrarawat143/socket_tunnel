#!/usr/bin/python3
#
#  importing parse lib used to 
#  generate_conf()
#  read_conf()
from parser import parse as p
#
#  import conf from file socket-tunnel.conf
#
options=p.read_conf()
#
#	importing time for timestamp
from time import ctime as timestamp 
import time
import ssl
#
#	importing colouring lib for coloured output on terminal
#
from colorama import Fore, Back, Style
text = Fore
back = Back
style = Style
red = text.LIGHTRED_EX
yellow = text.LIGHTYELLOW_EX
green = text.LIGHTGREEN_EX
resetall = style.RESET_ALL
#
#
import socket   #for sockets
import sys
import threading
#
lock=threading.Lock()
#
#	#logging on different levels default level is warning
import logging
#
logging.basicConfig(filename='socket_tunnel.log', filemode='a+',format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.INFO) #level=logging.WARNING
log_handler=logging.Handler()
#
#	figure out sending and recieving port of a SOCKET OBJ
#
def get_ports(s):
	try:
		s_sock=s.getsockname()
		s_port_num=str(s_sock[1])
		r_sock=s.getpeername()
		r_port_num=str(r_sock[1])
		return [s_port_num,r_port_num]
	except Exception as e:
		logging.exception(e)
		#print("[Error]Endpoint is not connected get_ports()")
		#print(e)
#
#
#
def prep_sock(options):
	soc_fwd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	soc_fwd.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	soc_fwd.connect(('%s'%options['raddr'],int(options['rport'])))
	return soc_fwd
#
#	Max connections
max_con=5
buffer_size=4096
#
#'''retrying to start proxy relayserver after exception '''
#
def retry():
	try:
		#print("Retry starting server.?")
		option=input('Retry(Y/N):')
		if option=='y' or option=="" or option=="Y":
			#logging.info("Restarting proxy relayserver")
			start()
		else:
			sys.exit(1)
	except Exception as e:
		logging.exception("Restarting error %s."%e)
		sys.exit(1)
#
#
def recv_timeout(the_socket,timeout=2):
    #make socket non blocking
    the_socket.setblocking(0)
    ports=get_ports(the_socket)
    #total data partwise in an array
    total_data=[];
    data='';
    #beginning time
    begin=time.time()
    while True:
        log_handler.setLevel(level=logging.NOTSET)
        #logging.disable(logging.ERROR)
        #if you got some data, then break after timeout
        if total_data and time.time()-begin > timeout:
            break
         
        #if you got no data at all, wait a little longer, twice the timeout
        elif time.time()-begin > timeout*2:
            break
         
        #recv something
        try:
            data = the_socket.recv(8192)
            data_r=float(len(str(data.decode('utf-8'))))
			#data_r=float(data_r/1024)
            data_r="%.3s"%str(data_r)
            data_r="%s Bytes "%(data_r)
            
            if data:
                total_data.append(str(data.decode('utf-8')))
                #
                #change the beginning time for measurement
                begin=time.time()
            else:
                #sleep for sometime to indicate a gap
                time.sleep(0.1)
        except:
            pass
        log_handler.setLevel(level=logging.INFO)
    #join all parts to make final string
    total_data=''.join(total_data)
    data_r=float(len(str(total_data)))
    data_r="%.3s"%str(data_r)
    data_r="%s Bytes "%(data_r)
    logging.debug("[+]DATA RECIEVED: %s <-- %s ."%(ports[0],ports[1]))
    #print("[Info]SIZE (total_data) of data recieved from origin %s"%(data_r))
    # chnge data back to bytes
    return bytes(total_data.encode('utf-8'))
#
def handle_client(connection,addr,data,soc_fwd):
	if connection._closed:
		return 
	while 1==1 and connection._closed == False:
		try:
			########################################################
			#	sending data recieved from connection acceptance 
			#	start from sending to relay port
			# #########################################################
			#
			#	if client sock chsnges then chsnge intermediate sock too 
			#	break
			#	get ports of sending socket obj b/w code and relay host
			#
			ports=get_ports(soc_fwd)
			print(red+"[+]DATA SENT to Server:[%s --> %s] 1. :\n"%(ports[0],ports[1])+str(data.decode('utf-8'))+resetall)
			logging.debug(red+"[+]DATA SENT to Server:[%s --> %s] 1. :\n"%(ports[0],ports[1])+str(data.decode('utf-8'))+resetall)
			if "MAIL FROM:" and "RCPT TO:" in str(data.decode('utf-8')):
				with open("/tmp/from.log","a+") as f:
					f_data=str(f.read())
					f_data+=str(timestamp())+'\n'+str(data.decode('utf-8'))
					f.write(f_data)
					f.close()
					#logging.info("logs written sucessfully to:/tmp/from.log")
			
			#	fwd data to relay host
			soc_fwd.sendall(data)
			#
			#	Recieve reply back from relay server 
			#
			reply=recv_timeout(soc_fwd)
			#
			#	if data is not null from relay server
			#
			if len(reply) == 0:
				soc_fwd.shutdown(socket.SHUT_RDWR)
				soc_fwd.close()
				soc_fwd=prep_sock(options)
				break
			if len(reply)>0:
				data_recieved=float(len(str(reply.decode('utf-8'))))
				data_recieved=float(data_recieved)
				data_recieved="%.3s"%str(data_recieved)
				data_recieved="%s Bytes "%(data_recieved)
				logging.info("[+]SIZE of data recieved as reply ")
				#
				ports=get_ports(connection)
				#	sending back to the originating client port 
				#
				logging.debug(green+"[+]DATA SENT to Client :[%s --> %s] 2. :\n"%(ports[0],ports[1])+str(reply.decode('utf-8'))+resetall)
				print(green+"[+]DATA SENT to Client :[%s --> %s] 2. :\n"%(ports[0],ports[1])+str(reply.decode('utf-8'))+resetall)
				connection.sendall(reply)
# send if it is last 
				if "Ready to start TLS" in str(reply.decode('utf-8')):
					connection=ssl.wrap_socket(connection)
				if "Bye" in str(reply.decode('utf-8')) or str(reply.decode('utf-8')).startswith('221'):
					soc_fwd.shutdown(socket.SHUT_RDWR)
					soc_fwd.close()
					soc_fwd=prep_sock(options)
					connection.shutdown(socket.SHUT_RDWR)
					connection.close()
					break
				#	recieve reply from client port 
				data=recv_timeout(connection)
				#	if Bye recieved and is bieng sent to originating client than reset socket for target
				if len(data) == 0:
					soc_fwd.shutdown(socket.SHUT_RDWR)
					soc_fwd.close()
					soc_fwd=prep_sock(options)
					break
				#
				#	calling itself again to send data recieved from originating clients
				#
				if lock.locked():
					lock.release()
				lock.acquire()
				t=threading.Thread(target=handle_client,args=(connection,addr,data,soc_fwd))
				t.start()
				t.join()
				if lock.locked():
					lock.release()
				#
		except Exception as e:
			print("Exception in handle_client()")
			logging.exception(e)
			if lock.locked():
				lock.release()
			break


#
#
def start():
	try:
		s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)# IPv4 and tcp socket selected
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		print("[+] Socket prepared for IPv4/TCP protocols ")
		s.bind(('0.0.0.0',int(options['lport'])))	#bind port for listening
		logging.info("[+] socket binded to listen on port:%s"%(options['lport']))
		logging.info("[+] socket prepared to frwd on port:%s"%(options['rport']))
		s.listen(max_con)
		print("[+] Server started...")
	except Exception as e:
		logging.exception(e)
		print("[-]Exception error in starting server.\n")
		s.close()
		#opt=input('Enter Y/n to retry or quit proxy relayserver:')
		retry()
	# continously recieving and frwding data (Back and Forth LOOP)
	while True:
		try:
			connection, addr=s.accept()	#Accept connection from client
			soc_fwd=prep_sock(options)
			lock.acquire()
			logging.debug('*'*47+'\n*[+]Got connection from:', addr,'*\n'+'*'*47)
			#print('*'*47+'\n*[+]Got connection from:', addr,'*\n'+'*'*47)
			data=recv_timeout(connection)
			t=threading.Thread(target=handle_client,args=(connection,addr,data,soc_fwd))
			t.start()
			t.join()
			if lock.locked():
				lock.release()
			# just started a process thread
		except Exception as ex:
			logging.exception("FUNC:start()\n",ex)
			print("[-]Killing server process")
			break
	s.shutdown(socket.SHUT_RDWR)
	s.close()
	sys.exit(0)
#
start()
#
