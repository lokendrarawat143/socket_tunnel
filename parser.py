#!/usr/bin/python3
#
import configparser 
#
class parse:
	def __init__(self):
		self.cfg=configparser.ConfigParser()
		self.options
	#   generate initial socket-tunnel.conf file
	#
	def generate_conf(self):
		#   DEFAULT section of config file
		cfg=configparser.ConfigParser()
		cfg['DEFAULT']={'laddr':'127.0.0.1',
				'lport':'1234',
				'raddr':'127.0.0.1',
				'rport':'2525',
				'logs':'/tmp/socket-tunnel.log'}
		#
		#   MANUAL (User-Defined) section 
		cfg['MANUAL']={'# Manually override default values.'
				'\n#laddr':'',
				'# '
				'\n#lport':'',
				'# '
				'\n#raddr':'',
				'# '
				'\n#rport':''}
		#  DEBUG additional info settings
		cfg['DEBUG']={'#logs':'/tmp/socket-tunnel.log'}
		#
		#  generate conf file with above configurations
		#
		with open('socket-tunnel.conf','w') as initial_conf_file:
			cfg.write(initial_conf_file)
#
#  reading configuration from .conf file
#
	def read_conf():
		cfg=configparser.ConfigParser()
		cfg.read('socket-tunnel.conf')
		manual=cfg['MANUAL']
		#
		#print(manual)
		all_options=['laddr','lport','raddr','rport','logs']
		d={}
		for key in all_options:
			#print('%s'%key,manual['%s'%key])
			d['%s'%key]='%s'%manual['%s'%key]
		return d
#parse.generate_conf(parse)
d=parse.read_conf()
