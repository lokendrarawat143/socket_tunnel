#!/usr/bin/python3
#
#  importing parse lib used to 
#  generate_conf()
#  read_conf()
from parser import parse as p
#
#  import conf from file socket-tunnel.conf
#
options=p.read_conf()
#
#	importing time for timestamp
from time import ctime as timestamp 
import time
import ssl
#
#	importing colouring lib for coloured output on terminal
#
from colorama import Fore, Back, Style
text = Fore
back = Back
style = Style
red = text.LIGHTRED_EX
yellow = text.LIGHTYELLOW_EX
green = text.LIGHTGREEN_EX
resetall = style.RESET_ALL
#
#
import socket   #for sockets
import sys
import threading
#
lock=threading.Lock()
#
#	#logging on different levels default level is warning
import logging
#
logging.basicConfig(filename='socket_tunnel.log', filemode='a+',format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.INFO) #level=logging.WARNING
#
#	figure out sending and recieving port of a SOCKET OBJ
#
def get_ports(s):
	try:
		s_sock=s.getsockname()
		s_port_num=str(s_sock[1])
		r_sock=s.getpeername()
		r_port_num=str(r_sock[1])
		return [s_port_num,r_port_num]
	except Exception as e:
		print("Endpoint is not connected : get_ports()")
		#logging.exception(e)
		#print("[Error]Endpoint is not connected get_ports()")
		#print(e)
#
#
#
def prep_sock(options):
	soc_fwd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	soc_fwd.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	soc_fwd.connect(('%s'%options['raddr'],int(options['rport'])))
	return soc_fwd
#
#print(soc_fwd.__getstate__)
#
#	Max connections
max_con=5
buffer_size=4096
#
#'''retrying to start proxy relayserver after exception '''
#
def retry():
	try:
		print("Retry starting server.?")
		option=input('(Y/N):')
		if option=='y' or option=="" or option=="Y":
			#logging.info("Restarting proxy relayserver")
			start()
		else:
			sys.exit(1)
	except KeyboardInterrupt:
		#logging.exception("Exiting on Keyboard Interrupt.")
		sys.exit(1)
#
#
def recv_timeout(the_socket,timeout=2):
    #make socket non blocking
    the_socket.setblocking(0)
    ports=get_ports(the_socket)
    #total data partwise in an array
    total_data=[];
    data='';
     
    #beginning time
    begin=time.time()
    while 1:
        #if you got some data, then break after timeout
        if total_data and time.time()-begin > timeout:
            break
         
        #if you got no data at all, wait a little longer, twice the timeout
        elif time.time()-begin > timeout*2:
            break
         
        #recv something
        try:
            data = the_socket.recv(8192)
            data_r=float(len(str(data.decode('utf-8'))))
			#data_r=float(data_r/1024)
            data_r="%.3s"%str(data_r)
            data_r="%s Bytes "%(data_r)
            
            if data:
                total_data.append(str(data.decode('utf-8')))
                #
                #change the beginning time for measurement
                begin=time.time()
            else:
                #sleep for sometime to indicate a gap
                time.sleep(0.1)
        except:
            pass
    #join all parts to make final string
    total_data=''.join(total_data)
    data_r=float(len(str(total_data)))
    data_r="%.3s"%str(data_r)
    data_r="%s Bytes "%(data_r)
    print("[+]DATA RECIEVED: %s <-- %s :\n"%(ports[0],ports[1]))
    #print("[Info]SIZE (total_data) of data recieved from origin %s"%(data_r))
    # chnge data back to bytes
    return bytes(total_data.encode('utf-8'))
#
def handle_client(connection,addr,data,soc_fwd):
	print("conn socket closed:",connection._closed)
	if connection._closed:
		return 
	while 1==1 and connection._closed == False:
		try:
			########################################################
			#	sending data recieved from connection acceptance 
			#	start from sending to relay port
			# #########################################################
			#
			#	if client sock chsnges then chsnge intermediate sock too 
			#	break
			#	get ports of sending socket obj b/w code and relay host
			#
			ports=get_ports(soc_fwd)
			print(red+"[+]DATA SENT :[%s --> %s] 1. :\n"%(ports[0],ports[1])+str(data.decode('utf-8'))+resetall)
			if "MAIL FROM:" and "RCPT TO:" in str(data.decode('utf-8')):
				#print('this is the place to write file logs')
				with open("/tmp/from.log","a+") as f:
					f_data=str(f.read())
					f_data+=str(timestamp())+'\n'+str(data.decode('utf-8'))
					f.write(f_data)
					f.close()
					print("logs written sucessfully ...")
			
			#	fwd data to relay host
			soc_fwd.sendall(data)
			#
			#	Recieve reply back from relay server 
			#
			reply=recv_timeout(soc_fwd)
			#
			#	if data is not null from relay server
			#
			if len(reply) == 0:
				#print("\nchk handle_client() while -> if len == 0 \n")
				soc_fwd.shutdown(socket.SHUT_RDWR)
				soc_fwd.close()
				soc_fwd=prep_sock(target_host,target_port)
				#connection.shutdown(socket.SHUT_RDWR)
				#connection.close()
				break
			if len(reply)>0:
				data_recieved=float(len(str(reply.decode('utf-8'))))
				data_recieved=float(data_recieved)
				data_recieved="%.3s"%str(data_recieved)
				data_recieved="%s Bytes "%(data_recieved)
				#print("[+]SIZE of data recieved as reply %s"%(data_recieved))
				#
				ports=get_ports(connection)
				#	sending back to the originating client port 
				#
				print(green+"[+]DATA SENT :[%s --> %s] 2. :\n"%(ports[0],ports[1])+str(reply.decode('utf-8'))+resetall)
				connection.sendall(reply)
# send if it is last 
				if "Ready to start TLS" in str(reply.decode('utf-8')):
					connection=ssl.wrap_socket(connection)
				if "Bye" in str(reply.decode('utf-8')) or str(reply.decode('utf-8')).startswith('221'):
					#print("\nchk handle_client() while -> if Bye \n")
					soc_fwd.shutdown(socket.SHUT_RDWR)
					soc_fwd.close()
					soc_fwd=prep_sock(target_host,target_port)
					connection.shutdown(socket.SHUT_RDWR)
					connection.close()
					break
				#	recieve reply from client port 
				#print("\nchk [1]handle_client() while -> if len > 0 \n")
				data=recv_timeout(connection)
				#	if Bye recieved and is bieng sent to originating client than reset socket for target
				if len(data) == 0:
					#print("\nchk handle_client() while -> if len == 0 \n")
					soc_fwd.shutdown(socket.SHUT_RDWR)
					soc_fwd.close()
					soc_fwd=prep_sock(target_host,target_port)
					break
				#
				#	calling itself again to send data recieved from originating clients
				#
				t=threading.Thread(target=handle_client,args=(connection,addr,data,soc_fwd))
				t.start()
				t.join()
				#
		except Exception as e:
			print(e)
			#print("[-]Error in handle client FUNC")
			print("[Info]Closing sockets...")
			break
	#print("\nchk handle_client() out of while true 0 \n")


#
#
def start():
	try:
		s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)# IPv4 and tcp socket selected
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		print("[+] Socket prepared for IPv4/TCP protocols ")
		s.bind(('0.0.0.0',lport))	#bind port for listening
		print("[+] socket binded to listen on port:%d"%(lport))
		s.listen(max_con)
		print("[+] Server started \n[+] Waiting for clients to connect...\n")
	except Exception as e:
		#	print the exception object e
		# or just a simple message 
		print("[-]Exception error in starting server.\n")
		print(e,'\n')
		s.close()
		#opt=input('Enter Y/n to retry or quit proxy relayserver:')
		retry()
	# continously recieving and frwding data (Back and Forth LOOP)
	while True:
		try:
			#print("\nchk 1\n")
			connection, addr=s.accept()	#Accept connection from client
			soc_fwd=prep_sock(target_host,target_port)
			lock.acquire()
			print('-'*50+'\n'+'*'*47+'\n*[+]Got connection from:', addr,'*\n'+'*'*47)
			data=recv_timeout(connection)
			#temp_socks={connection:soc_fwd}
			#print("\nchk 2\n")
			t=threading.Thread(target=handle_client,args=(connection,addr,data,soc_fwd))
			t.start()
			t.join()
			lock.release()
			# just started a process thread
		except Exception as e:
			print(e)
			print("[-]Killing server process")
			break
			#sys.exit(0)
	#connection.shutdown(socket.SHUT_RDWR)
	s.shutdown(socket.SHUT_RDWR)
	s.close()
#
#
start()
#
